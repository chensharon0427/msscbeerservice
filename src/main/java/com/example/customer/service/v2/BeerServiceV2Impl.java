package com.example.customer.service.v2;

import com.example.customer.dto.v2.BeerDtoV2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.example.customer.dto.v2.BeerStyleEnum.ALE;
import static com.example.customer.dto.v2.BeerStyleEnum.LAGER;

@Service
@Slf4j  //service +logger
public class BeerServiceV2Impl implements BeerServiceV2{

    @Override
    public BeerDtoV2 getBeerById(UUID beerId) {
        return BeerDtoV2.builder()
                .id(UUID.randomUUID())
                .beerName("Galaxy Cat")
                .beerStyle(LAGER)
                .build();
    }

    @Override
    public BeerDtoV2 saveNewBeer(BeerDtoV2 beerDtoV2) {
        return BeerDtoV2.builder()
                .id(UUID.randomUUID())
                .beerName("Heineken")
                .beerStyle(ALE)
                .upc(234L)
                .build();
    }

    @Override
    public void updateBeer(UUID beerId, BeerDtoV2 beerDtoV2) {
        //TODO imple
        log.debug("update beer......");
    }

    @Override
    public void deleteBeerById(UUID beerId) {
        log.debug("delete beer......");
    }
}
