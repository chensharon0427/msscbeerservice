package com.example.customer.controller;

import com.example.customer.dto.BeerDto;
import com.example.customer.service.BeerService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/api/v1/beer")
@RestController
public class BeerController {
    private BeerService beerService;

    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }
    @GetMapping("/{beerId}")
    public ResponseEntity<BeerDto> getBeer(@PathVariable("beerId") UUID beerId){
        BeerDto beerDto=beerService.getBeerById(beerId);
        return new ResponseEntity<>(beerDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity handlePost(@RequestBody BeerDto beerDto){
        BeerDto savedDto=beerService.saveNewBeer(beerDto);
        HttpHeaders headers= new HttpHeaders(); //implements MultiValueMap<String, String>,
        //TODO:add hostname to api: http://localhost:8089/
        headers.add("Location","/api/v1/beer/"+savedDto.getId().toString());
        return new ResponseEntity(headers,HttpStatus.CREATED);
    }

    //indenpotent
    //why not @RequestBody?
    @PutMapping("/{beerId}")
    public ResponseEntity handleUpdate(@PathVariable("beerId") UUID beerId, @RequestBody BeerDto beerDto){
        beerService.updateBeer(beerId,beerDto);
        return new ResponseEntity(HttpStatus.NO_CONTENT); //204, nothing went wrong
    }

    @DeleteMapping("/{beerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT) //new format, just as above
    public void handleDelete(@PathVariable("beerId") UUID beerId){
        beerService.deleteBeerById(beerId); // return void/nothing
    }

}
