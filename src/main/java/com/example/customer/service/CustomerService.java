package com.example.customer.service;

import com.example.customer.dto.CustomerDto;

import java.util.UUID;

//import com.example.customer.repository.CustomerRepository.CustomerRepository;


public interface CustomerService {

    public CustomerDto getCustomerById(UUID id);

    CustomerDto saveNewCustomer(CustomerDto customerDto);

    void updateCustomer(UUID customerId, CustomerDto customerDto);

    void deleteBeerById(UUID customerId);
}
