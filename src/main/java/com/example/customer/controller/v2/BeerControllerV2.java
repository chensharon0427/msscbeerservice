package com.example.customer.controller.v2;

import com.example.customer.dto.v2.BeerDtoV2;
import com.example.customer.service.v2.BeerServiceV2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/api/v2/beer")
@RestController
public class BeerControllerV2 {
    private BeerServiceV2 beerServiceV2;

    public BeerControllerV2(BeerServiceV2 beerServiceV2) {
        this.beerServiceV2 = beerServiceV2;
    }
    @GetMapping("/{beerId}")
    public ResponseEntity<BeerDtoV2> getBeer(@PathVariable("beerId") UUID beerId){
        BeerDtoV2 beerDtoV2=beerServiceV2.getBeerById(beerId);
        return new ResponseEntity<>(beerDtoV2, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity handlePost(@RequestBody BeerDtoV2 beerDtoV2){
        BeerDtoV2 savedDto=beerServiceV2.saveNewBeer(beerDtoV2);
        HttpHeaders headers= new HttpHeaders(); //implements MultiValueMap<String, String>,
        //TODO:add hostname to api: http://localhost:8089/
        headers.add("Location","/api/v1/beer/"+savedDto.getId().toString());
        return new ResponseEntity(headers,HttpStatus.CREATED);
    }

    //indenpotent
    //why not @RequestBody?
    @PutMapping("/{beerId}")
    public ResponseEntity handleUpdate(@PathVariable("beerId") UUID beerId, @RequestBody BeerDtoV2 beerDtoV2){
        beerServiceV2.updateBeer(beerId,beerDtoV2);
        return new ResponseEntity(HttpStatus.NO_CONTENT); //204, nothing went wrong
    }

    @DeleteMapping("/{beerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT) //new format, just as above
    public void handleDelete(@PathVariable("beerId") UUID beerId){
        beerServiceV2.deleteBeerById(beerId); // return void/nothing
    }
}
